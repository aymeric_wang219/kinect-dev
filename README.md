** Hello There!**

This work is part of a research work on the Kinect v1 from Microsoft. I was aiming to determine whether the Kinect could track hand movements of a surgeon well enough to be used as a Human-computer interface. This way, the surgeon could read through a patient file faster than traditional methods and that, right before immediate surgery.

I used OpenKinect libs and Processing 3 to come up with this code.
