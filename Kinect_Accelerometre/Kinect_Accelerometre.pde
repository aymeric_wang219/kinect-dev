// Daniel Shiffman
// All features test

// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/

import org.openkinect.freenect.*;
import org.openkinect.processing.*;
import processing.serial.*;

PrintWriter output;


Kinect kinect;

float deg;
float periodOfMeasurement = 5000; // milliseconds
float frequencyOfTilt = 1 ; // Hz
float numberOfSinus = 6 ;

boolean ir = false;
boolean colorDepth = true;
boolean mirror = false;


void setup() {
  size(500,500);
  kinect= new Kinect(this);
  kinect.initDepth();
  kinect.enableIR(ir);
  kinect.enableColorDepth(colorDepth);
  
  deg = kinect.getTilt();
  kinect.setTilt(0);
  
  output = createWriter("battement.txt");
  output.println("Time    Raw_Tilt_Data");
  output.flush();
}

void draw() {
  
  float initializedTime = millis();
  
  background(0);
  image(kinect.getDepthImage(),300,0);
  fill(255);
  text(
    "Framerate : " + int(frameRate),10, 515);
  deg = constrain(deg, 0, 30);
  
  while((millis()-initializedTime)<periodOfMeasurement) {
      if (0<=deg && deg<30 || deg == -30) { 
        float StarterTime=millis();
        while ((millis()-StarterTime)<(1/(numberOfSinus*frequencyOfTilt))) {
          kinect.setTilt(30);
          
          output.println(str(second()-initializedTime) + "   " + str(kinect.getTilt()));
          output.flush();
          
          int a=round(2000/frequencyOfTilt);
          delay(a);
          }
        }
      }
      
      if (-30<deg && deg<=0 || deg == 30) {
        float StarterTime=millis();
        while ((millis()-StarterTime)<(1/(numberOfSinus*frequencyOfTilt))) {
          kinect.setTilt(-30);
          
          output.println(str(second()-initializedTime) + "   " + str(kinect.getTilt()));
          output.flush();
          
          int a=round(2000/frequencyOfTilt);
          delay(a);
          }
        }
   }